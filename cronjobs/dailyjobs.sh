#!/bin/sh
# It is run every day from nm's crontab
# 53 23 * * * /srv/nm.debian.org/cronjobs/dailyjobs.sh

umask 022

# Update mailing lists
cd /srv/nm.debian.org/nm2

TMPFILE=`mktemp /srv/nm.debian.org/mail/nm-committee.include.XXXXXX`
./manage.py list_emails --procmail ctte > $TMPFILE
mv $TMPFILE /srv/nm.debian.org/mail/nm-committee.include

TMPFILE=`mktemp /srv/nm.debian.org/mail/am.include.XXXXXX`
./manage.py list_emails --procmail am > $TMPFILE
mv $TMPFILE /srv/nm.debian.org/mail/am.include

# Update changelogs
./manage.py index_changelogs

## Build NM graphs
#cd /srv/nm.debian.org/web/images
#../../bin/nm_graph

#!/bin/sh
# It is run every week from nm's crontab
# 56 23 * * sun /srv/nm.debian.org/cronjobs/weeklyjobs.sh

DATE=`date +'NM Report for Week Ending %d %b %Y'`
/srv/nm.debian.org/bin/weekrpt.pl --email | mailx -s "$DATE" -a "From: NM Front Desk <nm@debian.org>" debian-newmaint@lists.debian.org

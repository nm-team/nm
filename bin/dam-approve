#!/usr/bin/python

import os
import os.path
import sys
import pwd
import subprocess
import tempfile
import logging

log = logging.getLogger(sys.argv[0])

# Script environment
DEBHOME=os.environ.get("DEBHOME", os.path.expanduser("~/debian"))
pw = pwd.getpwuid(os.getuid())
ENV = dict(
    DEBHOME=DEBHOME,
    KEYRINGS=os.path.join(DEBHOME, "keyring.debian.org/keyrings"),
    DEBEMAIL=os.environ.get("DEBEMAIL", pw.pw_name + "@debian.org"),
    DEBFULLNAME=os.environ.get("DEBFULLNAME", pw.pw_gecos.split(",", 1)[0]),
    EDITOR=os.environ.get("EDITOR", "vi"),
)

TPL_HEADER = """\
From: {DEBFULLNAME} <{DEBEMAIL}>
To: Debian Keyring Maintainers <keyring@rt.debian.org>
Cc: Debian Account Managers <da-manager@debian.org>, {fullname} <{emailForward}>
Subject: [Debian RT] Account for {fullname}
"""

TPL_BODY_FORM = """\
  First name:      {cn}
  Middle name:     {mn}
  Last name:       {sn}
  Key fingerprint: {keyFingerPrint}
  Account:         {uid}
  Forward email:   {emailForward}
"""

TPL_BODY_SIG = """\

Thank you,

{DEBFULLNAME} (as DAM)
"""

TPL_BODY_DM = """\
Please make {fullname} (currently a DM) an uploading DD.

Key {keyFingerPrint} should be removed from the DM keyring and
added to the DD keyring.

""" + TPL_BODY_FORM + TPL_BODY_SIG

TPL_BODY_NODM = """\
Please make {fullname} (currently NOT a DM) an uploading DD.

Key {keyFingerPrint} should be added to the DD keyring.

""" + TPL_BODY_FORM

class Candidate(object):
    def __init__(self):
        self.info = dict()

    def read_am_form(self):
        # Map possible field names to LDAP names
        fieldmap = {
            "first name":       "cn",
            "middle name":      "mn",
            "last name":        "sn",
            "key fingerprint":  "keyFingerPrint",
            "account":          "uid",
            "forward email":    "emailForward",
        }
        print "Please paste the AM report Identification & Account Data form, end with an empty line:"
        while True:
            line = raw_input().strip()
            if not line: break
            if not ":" in line: continue
            key, val = line.split(":", 1)
            field = fieldmap.get(key.lower().strip(), None)
            if field is None:
                log.warning("""field not recognised: "%s", value: "%s".""", key, val)
                continue
            val = val.strip()
            if field == "keyFingerPrint":
                val = val.replace(" ", "")
            elif field == "mn":
                # Some people use "--" for missing
                if not val or val[0] == "-" or val == "n/a":
                    val = ""
            self.info[field] = val

        if not self.info.get("mn", None):
            self.info["fullname"] = " ".join((self.info[x] for x in ["cn", "sn"]))
            self.info["mn"] = ""
        else:
            self.info["fullname"] = " ".join((self.info[x] for x in ["cn", "mn", "sn"]))

    def check_dm(self):
        dm_keyring = os.path.join(ENV["KEYRINGS"], "debian-maintainers.gpg")

        cmd = [
            "gpg",
            "-q", "--no-options", "--no-default-keyring", "--no-auto-check-trustdb", "--trust-model", "always",
            "--keyring", dm_keyring,
            "--with-colons", "--with-fingerprint", "--list-keys", self.info["keyFingerPrint"],
        ]
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.dm_check_stdout, self.dm_check_stderr = proc.communicate()
        self.dm_check_result = proc.wait()
        self.is_dm = None
        if self.dm_check_result == 0:
            self.is_dm = True
        elif self.dm_check_result == 2:
            self.is_dm = False
        else:
            raise RuntimeError("gpg exited with status %d: %s" % (self.dm_check_result, self.dm_check_stderr.strip()))
        return self.is_dm

    def build_email(self):
        #import textwrap

        context = ENV.copy()
        context.update(self.info)

        self.mail_header = TPL_HEADER.format(**context).rstrip()
        if self.is_dm:
            tpl = TPL_BODY_DM
        else:
            tpl = TPL_BODY_NODM
        self.mail_body = tpl.format(**context)

    def edit_string(self, s):
        # Write the string to a temp file
        # We cannot rely on autodelete here, because the editor may not edit
        # the file in place
        f = tempfile.NamedTemporaryFile(delete=False)
        print >>f, s
        f.close()

        try:
            # Invoke $EDITOR
            res = subprocess.call([ENV["EDITOR"], f.name])
            if res != 0:
                low.warning("%s failed: keeping the old value", ENV["EDITOR"])
                return s

            # Read back the file and return its content
            with open(f.name) as fd:
                return fd.read()
        finally:
            os.unlink(f.name)

    def edit_header(self):
        self.mail_header = self.edit_string(self.mail_header)

    def edit_body(self):
        self.mail_body = self.edit_string(self.mail_body)

    def run_keycheck(self):
        cmd = ["../nm-templates/keycheck.sh", self.info["keyFingerPrint"]]
        res = subprocess.call(cmd)
        return res == 0

    def send(self):
        # Sign the body
        cmd = ["gpg", "--clearsign"]
        keyid = os.environ.get("DEBSIGN_KEYID", None)
        if keyid:
            cmd += ["--default-key", keyid]
        proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate(self.mail_body)
        result = proc.wait()
        if result != 0:
            log.error("gpg exited with status %d: %s", result, stderr.strip())
            return False
        body = stdout

        # Write the mail to a temp file
        with tempfile.NamedTemporaryFile() as fd:
            print >>fd, self.mail_header
            print >>fd
            print >>fd, stdout
            fd.flush()
            res = subprocess.call(["mutt", "-H", fd.name])
            if res != 0:
                log.error("mutt exited with status %d", res)
                return False
        return True



def menu():
    while True:
        print
        print "Please choose an action:"
        print " h: Edit header"
        print " b: Edit body"
        print " k: Run keycheck"
        print " s: Sign, send with mutt, and quit"
        print " q: Quit"
        print "> ",
        val = raw_input().strip()
        if val:
            return val

def make_dd():
    cand = Candidate()
    cand.read_am_form()
    cand.check_dm()
    cand.build_email()
    while True:
        print "v" * 20, "draft email", "v" * 20
        print cand.mail_header
        print
        print cand.mail_body
        print "^" * 20, "draft email", "^" * 20
        action = menu()
        if action == "h":
            cand.edit_header()
        elif action == "b":
            cand.edit_body()
        elif action == "k":
            cand.run_keycheck()
            print "[Press enter to go back to the mail draft]"
            raw_input()
        elif action == "s":
            if cand.send():
                break
        elif action == "q":
            break




if __name__ == "__main__":
    from optparse import OptionParser
    import sys

    VERSION="1.8"

    class Parser(OptionParser):
        def error(self, msg):
            sys.stderr.write("%s: error: %s\n\n" % (self.get_prog_name(), msg))
            self.print_help(sys.stderr)
            sys.exit(2)

    parser = Parser(usage="usage: %prog [options]",
                    version="%prog "+ VERSION,
                    description="Detects what tag combinations make sense for the current system.")
    parser.add_option("-q", "--quiet", action="store_true",
                      help="quiet mode: only output errors.")
    parser.add_option("-v", "--verbose", action="store_true",
                      help="verbose mode: output progress and non-essential information.")
    parser.add_option("-d", "--debug", action="store_true",
                      help="debug mode: output debug informationn.")
    (opts, args) = parser.parse_args()

    #FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
    FORMAT = "%(message)s"
    if opts.quiet:
        logging.basicConfig(level=logging.ERROR, stream=sys.stderr, format=FORMAT)
    elif not opts.verbose:
        logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
    elif opts.debug:
        logging.basicConfig(level=logging.DEBUG, stream=sys.stderr, format=FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

    make_dd()

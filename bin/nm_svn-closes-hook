#!/usr/bin/perl -w

# Find bugs closed by this commit and close them in the BTS
# Copyright © 2008 Christoph Berg <myon@debian.org>
# based on:

# svn-tag-pending-bugs
# Find bugs closed by this commit and tag them pending in the BTS
#
# Synopsis: $0 repos_path revision
#
# Copyright © 2008 Damyan Ivanov <dmn@debian.org>
#
# This program is free software. You may distribute it under
# the terms of Perl

use strict;
use Mail::Send;
use File::Basename qw(basename);
use Sys::Hostname::Long;

my( $repo_path, $revision ) = @ARGV;

die "missing repository and revision arguments" unless ($revision and $revision);

my $log = `svnlook log -r $revision $repo_path`;

my @bugs;
my $log_copy = $log;
while( $log_copy =~ s/(closes:\s*(?:bug)?\#\s*\d+(?:,\s*(?:bug)?\#\s*\d+)*)//is )
{
  my $match = $1;
  push @bugs, $1 while $match =~ s/#(\d+)//;
}

exit unless @bugs;

my @who = getpwuid($<);
my $author = $who[0];
my $author_name = $who[6];
my $hostname = hostname_long;

my $msg = Mail::Send->new(
  Subject => "nm.debian.org bug fixed in revision $revision",
);
$msg->add('From', "$author_name <$author\@$hostname>");
$msg->add('X-Mailer', (basename $0).' running on '.hostname_long);
$msg->add('X-Svn-Repository', $repo_path);
$msg->add('X-Debian', 'nm.debian.org');
$msg->add('MIME-Version', '1.0');
$msg->add('Content-Type', 'text/plain; charset=utf-8');
$msg->add('Content-Transfer-Encoding', '8bit');

$msg->to(
    join(
        ', ',
        map(
            ("$_-done\@bugs.debian.org"),
            @bugs )
    )
);

my $out = $msg->open();
$out->print("Version: $revision\n\n");
$out->print("This bug was closed by $author_name ($author) in SVN revision $revision.\n");
$out->print("Note that it might take some time until the nm.debian.org code has\n");
$out->print("been updated and cronjobs have picked up changed data.\n\n");
$out->print("Commit message:\n\n$log\n");
$out->close;

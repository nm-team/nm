# -*- coding: utf-8 -*-

from NM.config import MAIL_FROM, SMTP_HOST, SMTP_PORT
from email.mime.text import MIMEText
from email.header import Header

import smtplib
import types

__author__ = "Bernd Zeimetz <bzed@debian.org>"
__license__ = """
    Copyright (C) 2009  Bernd Zeimetz <bzed@debian.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

def sendmail(to, subject, body, sender=MAIL_FROM, encoding='utf-8', **kwargs):
    
    """
    Simple wrapper around the functions needed to send an email.
    @param to: Mail recipient(s)
    @type to: List, tuple or string.
    @param subject: Subject of the mail.
    @type subject: string 
    @param sender: From address, defaults to NM.config.MAIL_FROM
    @type sender: string
    @param encoding: Encoding used in the mail, defaults to (and should always
                     be) utf-8.
    @type encoding: string
    
    Also it is possible to specify additional headers as parameter to
    this function.    
    """
    
    msg = MIMEText(body, 'plain', encoding)
    sender = Header(sender, encoding)
    subject = Header(subject, encoding)
    
    if type(to) == types.ListType or type(to) == types.TupleType:
        to = (Header(x, encoding) for x in to)
        msg['To'] = ', '.join(to)
    else:
        msg['To'] = Header(to, encoding)
    msg['Subject'] = subject 
    msg['From'] = sender 

    for key, value in kwargs.iteritems():
        msg[key]=Header(value, encoding)
    
    smtp = smtplib.SMTP(SMTP_HOST, SMTP_PORT)
    smtp.sendmail(sender, to, msg.as_string())
    smtp.quit()

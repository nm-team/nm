# -*- coding: utf-8 -*-

""" This class contains various defaults we use """

SMTP_HOST = 'localhost'
SMTP_PORT = 25
MAIL_ADDRESS = 'nm@debian.org'
MAIL_FROM = 'Debian New Member Frontdesk <%s>' %(MAIL_ADDRESS, )

DB_DRIVER = 'postgres'
DB_DATABASE = 'newmaint'
DB_DEBUG = False

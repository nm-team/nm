# -*- coding: utf-8 -*-

import sqlalchemy.orm

from NM.config import DB_DRIVER, DB_DATABASE, DB_DEBUG

__author__ = "Bernd Zeimetz <bzed@debian.org>"
__license__ = """
    Copyright (C) 2009  Bernd Zeimetz <bzed@debian.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""


engine = sqlalchemy.create_engine('%s:///%s'
               %(DB_DRIVER, DB_DATABASE),
               echo = DB_DEBUG)

metadata = sqlalchemy.MetaData()
metadata.bind = engine


manager = sqlalchemy.Table('manager', metadata, autoload=True)
class Manager(object):
    pass
sqlalchemy.orm.mapper(Manager, manager)


applicant = sqlalchemy.Table('applicant', metadata, autoload=True)
class Applicant(object):
    pass
sqlalchemy.orm.mapper(Applicant, applicant)


log = sqlalchemy.Table('log', metadata, autoload=True)
#class Log(object):
#    pass
#sqlalchemy.orm.mapper(Log, log)

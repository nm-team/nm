# -*- coding: utf-8 -*-

import mako

__author__ = "Bernd Zeimetz <bzed@debian.org>"
__license__ = """
    Copyright (C) 2009  Bernd Zeimetz <bzed@debian.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""



class TemplateException(Exception):
    """
    TemplateException will be raised in case rendering
    the template fails.
    """
    pass

class Template(object):
    """
    The Template object is a wrapper around mako.TemplateLookup 
    and provides sane defaults for most options.
    """
    def __init__(self, template_dirs, input_encoding='utf-8',
                 output_encoding='utf-8',
                 encoding_errors='replace',
                 module_directory='/tmp/nm.d.o',
                 filesystem_checks=True):
        """
        @param template_dirs: directories where templates should be looked up
        @type template_dirs: list
        @param input_encoding: encoding used in templates
        @type input_encoding: string
        @param output_encoding: encoding the output should be converted to
        @type output_encoding: string
        @param encoding_errors: what to do when encoding errors happen
        @type encoding_errors: string  
        @param module_directory: Tempdir to store cached templates
        @type module_directory: string
        @param filesystem_checks: Check filesystem for template modifications
        @type filesystem_checks: bool 
        """
        self.template_lookup = mako.lookup.TemplateLookup(
                 directories=template_dirs,
                 input_encoding=input_encoding,
                 output_encoding=output_encoding,
                 encoding_errors=encoding_errors,
                 module_directory=module_directory,
                 filesystem_checks=filesystem_checks)
    
    def render(self, templatename, **kwargs):
        """
        Render a template and return the result.
        
        @param templatename: name of the template which should be looked up
        @type templatename: string
        @param kwargs: template context
        """
        try:
            template = self.template_lookup.get_template(templatename)
            return template.render(**kwargs)
        except:
            raise TemplateException(
                    mako.exceptions.text_error_template().render())

